Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: @openpgp/seek-bzip
Upstream-Contact: https://github.com/cscott/seek-bzip/issues
Source: https://github.com/cscott/seek-bzip#readme

Files: *
Copyright: 2011-2015, C. Scott Ananian, Eli Skeggs, Kevin Kwok
License: Expat

Files: lib/bitreader.js
Copyright: 2012 Eli Skeggs
License: GNU-LGPL

Files: lib/crc32.js
Copyright: 2013 C. Scott Ananian
Comment: Based port of CRC32.java from the jbzip2 implementation at
 code.google.com/p/jbzip2 which is: Copyright (c) 2011 Matthew Francis
License: Expat

Files: lib/index.js
Copyright: 2013 C. Scott Ananian,
 2012 Eli Skeggs
 2011 Kevin Kwok
License: GNU-LGPL

Files: debian/*
Copyright: 2022, sandra uwah <sandrauwah282@gmail.com>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GNU-LGPL
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, see
 http://www.gnu.org/licenses/lgpl-2.1.html
 .
 Adapted from bzip2.js, copyright 2011 antimatter15 (antimatter15@gmail.com).
 .
 Based on micro-bunzip by Rob Landley (rob@landley.net).
 .
 Based on bzip2 decompression code by Julian R Seward (jseward@acm.org),
 which also acknowledges contributions by Mike Burrows, David Wheeler,
 Peter Fenwick, Alistair Moffat, Radford Neal, Ian H. Witten,
 Robert Sedgewick, and Jon L. Bentley
